#pragma once
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define NTDDI_VERSION NTDDI_WIN7

/* C++ & STD */
#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>
#include <codecvt>
#include <fstream>
#include <ctime>

/* BOOST */
#include <boost/asio/connect.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/system/system_error.hpp>
#include <boost/asio/write.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

/* CPP_REST */
#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#include <cpprest/http_client.h>
#include <cpprest/containerstream.h>

/* C Header */
#include <stdlib.h>
#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>
#include <tlhelp32.h>
#include <shellapi.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


static std::wstring StringToWString(const std::string &s)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.from_bytes(s);
}

static std::string WStringToString(const std::wstring &ws)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.to_bytes(ws);
}

using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;
using boost::asio::deadline_timer;
using boost::asio::ip::tcp;
using boost::lambda::bind;
using boost::lambda::var;
using boost::lambda::_1;


#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))



class TCPclient
{
public:
	TCPclient() : m_Socket(m_Service), m_DeadLine(m_Service)
	{
		m_DeadLine.expires_at(boost::posix_time::pos_infin);

		check_deadline();
	}

	static int GetAdapInfo(std::string& strIP);
	static bool CheckPort(std::vector<unsigned int>& vPID, int nPort);
	static bool GetPID(std::string strProgram, std::vector<unsigned int>& vPID);
	void Connect(boost::asio::ip::tcp::resolver::iterator endpoint, boost::posix_time::time_duration timeout);
	std::string Read(boost::posix_time::time_duration timeout);
	void Write(const std::string& data, boost::posix_time::time_duration timeout);
	bool Init(const std::string& program, const std::string& host, const std::string& port);
	

	std::string PingPong(const std::wstring strSend);
	std::string PingPong(const std::string strSend);

	void Close();

private:
	void check_deadline();

public:
	boost::asio::io_service m_Service;
	tcp::socket m_Socket;
	deadline_timer m_DeadLine;
	boost::asio::streambuf m_ReadBuf;
	boost::asio::ip::tcp::resolver::iterator m_Endpoint;
};
