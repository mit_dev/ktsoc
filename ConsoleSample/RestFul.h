#pragma once
#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#include <cpprest/http_client.h>
#include <cpprest/containerstream.h>
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;
using namespace web::http::experimental::listener;


#include "TCPclient.h"
#include <iostream>
#include <map>
#include <set>
#include <string>

//#include "Windows.h"

#define TRACE(msg)            std::wcout << msg
#define TRACE_ACTION(a, k, v) std::wcout << a << L" (" << k << L", " << v << L")\n"

namespace restful
{
	class CRestClient
	{
	public:
		CRestClient();
		virtual ~CRestClient();

		void init(std::wstring strURI);
		void clear();
		pplx::task<http_response> makeTask(method mtd, utility::string_t path, web::json::value* jv = nullptr);

	private:
		http::uri m_uri;
		http_client* m_pClient;
	};
	class CRestServer
	{
	public:

		std::wstring getURI() const;
		std::string runServer(std::wstring strURI, TCPclient* pTCPSoc, CRestClient* pCRest, utility::string_t reqPath);

		void display_json(json::value const & jvalue, utility::string_t const & prefix) { std::wcout << prefix << jvalue.serialize() << std::endl; }

		void handleGet(http_request request);
		void handlePost(http_request request);
		void handlePut(http_request request);
		void handleReq(http_request request, std::function<void(json::value const &, json::value &)> action);
		void handleDel(http_request request);

	private:
		http::uri m_uri;
		http_listener* m_pListener;
		TCPclient* m_pTCPSoc;
		CRestClient* m_pCRest;
		utility::string_t m_reqPath;
	};

	
}
