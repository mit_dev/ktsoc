#include "TCPclient.h"
#include <windows.h>

int TCPclient::GetAdapInfo(std::string& strIP)
{
	strIP = "";
	/* Declare and initialize variables */

		// It is possible for an adapter to have multiple
		// IPv4 addresses, gateways, and secondary WINS servers
		// assigned to the adapter. 
		//
		// Note that this sample code only prints out the 
		// first entry for the IP address/mask, and gateway, and
		// the primary and secondary WINS server for each adapter. 

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	UINT i;

	/* variables used to print DHCP time info */
	struct tm newtime;
	char buffer[32];
	errno_t error;

	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO *)MALLOC(sizeof(IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		printf("Error allocating memory needed to call GetAdaptersinfo\n");
		return 1;
	}
	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		FREE(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *)MALLOC(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			printf("Error allocating memory needed to call GetAdaptersinfo\n");
			return 1;
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
		pAdapter = pAdapterInfo;
		while (pAdapter) {
			printf("\tComboIndex: \t%d\n", pAdapter->ComboIndex);
			printf("\tAdapter Name: \t%s\n", pAdapter->AdapterName);
			printf("\tAdapter Desc: \t%s\n", pAdapter->Description);
			printf("\tAdapter Addr: \t");
			for (i = 0; i < pAdapter->AddressLength; i++) {
				if (i == (pAdapter->AddressLength - 1))
					printf("%.2X\n", (int)pAdapter->Address[i]);
				else
					printf("%.2X-", (int)pAdapter->Address[i]);
			}
			printf("\tIndex: \t%d\n", pAdapter->Index);
			printf("\tType: \t");
			switch (pAdapter->Type) {
			case MIB_IF_TYPE_OTHER:
				printf("Other\n");
				break;
			case MIB_IF_TYPE_ETHERNET:
				printf("Ethernet\n");
				break;
			case MIB_IF_TYPE_TOKENRING:
				printf("Token Ring\n");
				break;
			case MIB_IF_TYPE_FDDI:
				printf("FDDI\n");
				break;
			case MIB_IF_TYPE_PPP:
				printf("PPP\n");
				break;
			case MIB_IF_TYPE_LOOPBACK:
				printf("Lookback\n");
				break;
			case MIB_IF_TYPE_SLIP:
				printf("Slip\n");
				break;
			default:
				printf("Unknown type %ld\n", pAdapter->Type);
				break;
			}

			printf("\tIP Address: \t%s\n",
				pAdapter->IpAddressList.IpAddress.String);
			strIP = pAdapter->IpAddressList.IpAddress.String;
			printf("\tIP Mask: \t%s\n", pAdapter->IpAddressList.IpMask.String);

			printf("\tGateway: \t%s\n", pAdapter->GatewayList.IpAddress.String);
			printf("\t***\n");

			if (pAdapter->DhcpEnabled) {
				printf("\tDHCP Enabled: Yes\n");
				printf("\t  DHCP Server: \t%s\n",
					pAdapter->DhcpServer.IpAddress.String);

				printf("\t  Lease Obtained: ");
				/* Display local time */
				error = _localtime32_s(&newtime, (__time32_t*)&pAdapter->LeaseObtained);
				if (error)
					printf("Invalid Argument to _localtime32_s\n");
				else {
					// Convert to an ASCII representation 
					error = asctime_s(buffer, 32, &newtime);
					if (error)
						printf("Invalid Argument to asctime_s\n");
					else
						/* asctime_s returns the string terminated by \n\0 */
						printf("%s", buffer);
				}

				printf("\t  Lease Expires:  ");
				error = _localtime32_s(&newtime, (__time32_t*)&pAdapter->LeaseExpires);
				if (error)
					printf("Invalid Argument to _localtime32_s\n");
				else {
					// Convert to an ASCII representation 
					error = asctime_s(buffer, 32, &newtime);
					if (error)
						printf("Invalid Argument to asctime_s\n");
					else
						/* asctime_s returns the string terminated by \n\0 */
						printf("%s", buffer);
				}
			}
			else
				printf("\tDHCP Enabled: No\n");

			if (pAdapter->HaveWins) {
				printf("\tHave Wins: Yes\n");
				printf("\t  Primary Wins Server:    %s\n",
					pAdapter->PrimaryWinsServer.IpAddress.String);
				printf("\t  Secondary Wins Server:  %s\n",
					pAdapter->SecondaryWinsServer.IpAddress.String);
			}
			else
				printf("\tHave Wins: No\n");
			pAdapter = pAdapter->Next;
			printf("\n");
		}
	}
	else {
		printf("GetAdaptersInfo failed with error: %d\n", dwRetVal);

	}
	if (pAdapterInfo)
		FREE(pAdapterInfo);

	return 0;
}

bool TCPclient::CheckPort(std::vector<unsigned int>& vPID, int nPort)
{
	// Declare and initialize variables
	PMIB_TCPTABLE2 pTcpTable;
	ULONG ulSize = 0;
	DWORD dwRetVal = 0;

	pTcpTable = (MIB_TCPTABLE2 *)MALLOC(sizeof(MIB_TCPTABLE2));
	if (pTcpTable == NULL) {
		std::cout << "Error allocating memory" << std::endl;
		return false;
	}

	ulSize = sizeof(MIB_TCPTABLE);
	// Make an initial call to GetTcpTable2 to
	// get the necessary size into the ulSize variable
	if ((dwRetVal = GetTcpTable2(pTcpTable, &ulSize, TRUE)) == ERROR_INSUFFICIENT_BUFFER)
	{
		FREE(pTcpTable);
		pTcpTable = (MIB_TCPTABLE2 *)MALLOC(ulSize);
		if (pTcpTable == NULL) {
			std::cout << "Error allocating memory" << std::endl;
			return false;
		}
	}

	// Make a second call to GetTcpTable2 to get
	// the actual data we require
	if ((dwRetVal = GetTcpTable2(pTcpTable, &ulSize, TRUE)) == NO_ERROR)
	{
		int nCnt = 0;
		for (int i = 0; i < (int)pTcpTable->dwNumEntries; i++)
		{
			char szLocalAddr[128];
			char szRemoteAddr[128];

			int nLocalPort = ntohs((u_short)pTcpTable->table[i].dwLocalPort);
			int nRemotePort = ntohs((u_short)pTcpTable->table[i].dwRemotePort);
			struct in_addr IpAddr;

			IpAddr.S_un.S_addr = (u_long)pTcpTable->table[i].dwLocalAddr;
			strcpy_s(szLocalAddr, sizeof(szLocalAddr), inet_ntoa(IpAddr));
			//printf("\tTCP[%d] Local Addr: %s\n", i, szLocalAddr);
			//printf("\tTCP[%d] Local Port: %d \n", i, nLocalPort);

			IpAddr.S_un.S_addr = (u_long)pTcpTable->table[i].dwRemoteAddr;
			strcpy_s(szRemoteAddr, sizeof(szRemoteAddr), inet_ntoa(IpAddr));
			//printf("\tTCP[%d] Remote Addr: %s\n", i, szRemoteAddr);
			//printf("\tTCP[%d] Remote Port: %d\n", i, nRemotePort);

			/*if (nLocalPort == nPort && nPID == 0)
			{
				nPID = pTcpTable->table[i].dwOwningPid;
				nCnt++;
			}*/
			for (auto pid : vPID)
			{
				if (nLocalPort == nPort && pTcpTable->table[i].dwOwningPid == pid)
				{
					/*printf("\tTCP[%d] Local Addr: %s\n", i, szLocalAddr);
					printf("\tTCP[%d] Local Port: %d \n", i, nLocalPort);

					printf("\tTCP[%d] Remote Addr: %s\n", i, szRemoteAddr);
					printf("\tTCP[%d] Remote Port: %d\n", i, nRemotePort);*/
					FREE(pTcpTable);
					return true;
				}
			}

		}
	}
	
	FREE(pTcpTable);
	return false;
}

bool TCPclient::GetPID(std::string strProgram, std::vector<unsigned int>& vPID)
{
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 processEntry32;

	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		exit(EXIT_FAILURE);
	}

	processEntry32.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(hProcessSnap, &processEntry32))
	{
		CloseHandle(hProcessSnap);
		exit(EXIT_FAILURE);
	}

	while (Process32Next(hProcessSnap, &processEntry32))
	{
		std::wstring wstrName = L"";

		wstrName.assign(strProgram.begin(), strProgram.end());

		if (wstrName == processEntry32.szExeFile)
		{
			vPID.push_back(processEntry32.th32ProcessID);
		}
	}

	return vPID.size() > 0 ? true : false;
}

void TCPclient::Connect(boost::asio::ip::tcp::resolver::iterator endpoint, boost::posix_time::time_duration timeout)
{
	m_DeadLine.expires_from_now(timeout);

	boost::system::error_code ec = boost::asio::error::would_block;

	boost::asio::async_connect(m_Socket, endpoint, var(ec) = _1);

	do m_Service.run_one(); while (ec == boost::asio::error::would_block);

	if (ec || !m_Socket.is_open())
		throw boost::system::system_error(
			ec ? ec : boost::asio::error::operation_aborted);
}

std::string TCPclient::Read(boost::posix_time::time_duration timeout)
{
	m_DeadLine.expires_from_now(timeout);

	boost::system::error_code ec = boost::asio::error::would_block;

	boost::asio::async_read_until(m_Socket, m_ReadBuf, '\n', var(ec) = _1);

	do m_Service.run_one(); while (ec == boost::asio::error::would_block);

	if (ec)
		throw boost::system::system_error(ec);

	std::string strRes;
	std::string line;
	std::istream is(&m_ReadBuf);
	while (std::getline(is, line))
	{
		strRes.append(line);
	}
	return strRes;
}

void TCPclient::Write(const std::string& data, boost::posix_time::time_duration timeout)
{
	std::string temp = data;

	m_DeadLine.expires_from_now(timeout);

	boost::system::error_code ec = boost::asio::error::would_block;

	boost::asio::async_write(m_Socket, boost::asio::buffer(temp), var(ec) = _1);

	do m_Service.run_one(); while (ec == boost::asio::error::would_block);

	if (ec)
		throw boost::system::system_error(ec);
}

bool TCPclient::Init(const std::string& program, const std::string& host, const std::string& port)
{
	std::vector<unsigned int> vPID;

	while (true)
	{
		if (GetPID(program, vPID)) break;
	}
	

	while (true)
	{
		if (CheckPort(vPID, std::stoi(port))) break;
	}

	boost::asio::ip::tcp::resolver resolver(m_Service);
	boost::asio::ip::tcp::resolver::query query(host, port);
	m_Endpoint = resolver.resolve(query);

	Connect(m_Endpoint, boost::posix_time::minutes(60));

	m_Socket.set_option(tcp::socket::reuse_address(true));

	return true;
}

std::string TCPclient::PingPong(const std::wstring wstrSend)
{
	return PingPong(WStringToString(wstrSend));
}

std::string TCPclient::PingPong(const std::string strSend)
{
	std::cout << "Send : " << strSend << std::endl;

	boost::posix_time::ptime time_sent = boost::posix_time::microsec_clock::universal_time();

	Write(strSend, boost::posix_time::minutes(1440));

	std::string strRead;

	strRead = Read(boost::posix_time::minutes(1440));

	boost::posix_time::ptime time_received = boost::posix_time::microsec_clock::universal_time();

	std::cout << "Recv : " << strRead << std::endl;

	//std::cout << "Round trip time: ";
	//std::cout << (time_received - time_sent).total_seconds();
	//std::cout << " (s)\n";

	return strRead;
}

void TCPclient::Close()
{
	m_Socket.shutdown(boost::asio::socket_base::shutdown_both);
	m_Socket.close();
}

void TCPclient::check_deadline()
{
	if (m_DeadLine.expires_at() <= deadline_timer::traits_type::now())
	{
		boost::system::error_code ignored_ec;
		m_Socket.close(ignored_ec);

		m_DeadLine.expires_at(boost::posix_time::pos_infin);
	}

	m_DeadLine.async_wait(bind(&TCPclient::check_deadline, this));
}
