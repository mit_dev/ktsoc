#include "Restful.h"

using namespace restful;

CRestClient::CRestClient()
{
	m_pClient = nullptr;
}

CRestClient::~CRestClient()
{
	clear();
}

void CRestClient::init(std::wstring strURI)
{
	clear();
	m_pClient = new http_client(strURI);
}

void CRestClient::clear()
{
	if (m_pClient) delete m_pClient;
	m_pClient = nullptr;
}

pplx::task<web::http::http_response> CRestClient::makeTask(method mtd, utility::string_t path, web::json::value* jv)
{
	if (m_pClient)
	{
		if (jv == nullptr)
			return m_pClient->request(mtd, path);
		else
			return m_pClient->request(mtd, path, *jv);
	}
	else
		return pplx::task<web::http::http_response>::task();
}


std::wstring CRestServer::getURI() const
{
	return m_uri.path();
}



void CRestServer::handleGet(http_request request)
{
	TRACE(L"\nhandle GET\n");
	
	handleReq(
		request,
		[&](json::value const & jvalue, json::value & answer)
	{

	});
}

void CRestServer::handlePost(http_request request)
{
	TRACE("\nhandle POST\n");
	
		handleReq(
			request,
			[&](json::value const & jvalue, json::value & answer)
		{
			std::string strRes = m_pTCPSoc->PingPong(WStringToString(jvalue.serialize()));
			answer = json::value::string(StringToWString(strRes));
			m_pCRest->makeTask(methods::PUT, m_reqPath, &answer)
				.then([&](http_response response)
			{
				stringstreambuf buffer;
				size_t sz = response.body().read_to_end(buffer).get();
			}).wait();
		});
}



void CRestServer::handlePut(http_request request)
{
	TRACE("\nhandle PUT\n");
	
		handleReq(
			request,
			[&](json::value const & jvalue, json::value & answer)
		{
			
		});
}

void CRestServer::handleReq(http_request request, std::function<void(json::value const &, json::value &)> action)
{
	auto answer = json::value::object();
		request
			.extract_json()
			.then([&](pplx::task<json::value> task) {
			try
			{
				auto const & jvalue = task.get();
				display_json(jvalue, L"REST REQ: ");
	
				if (!jvalue.is_null())
				{
					action(jvalue, answer);
				}
			}
			catch (http_exception const & e)
			{
				std::wcout << e.what() << std::endl;
			}
		})
			.wait();
	
	
		display_json(answer, L"REST RES: ");
	
		request.reply(status_codes::OK, answer);
}

void CRestServer::handleDel(http_request request)
{
	TRACE("\nhandle DEL\n");

	handleReq(
		request,
		[&](json::value const & jvalue, json::value & answer)
	{
		
	});
}

std::string CRestServer::runServer(std::wstring strURI, TCPclient* pTCPSoc, CRestClient* pCRest, utility::string_t reqPath)
{
	m_uri = strURI;
	m_pTCPSoc = pTCPSoc;
	m_pCRest = pCRest;
	m_reqPath = reqPath;
	std::string strRes; strRes.clear();
	m_pListener = new http_listener(strURI);
	
	m_pListener->support(methods::GET,  std::bind(&CRestServer::handleGet , this, std::placeholders::_1));
	m_pListener->support(methods::POST, std::bind(&CRestServer::handlePost, this, std::placeholders::_1));
	m_pListener->support(methods::PUT,  std::bind(&CRestServer::handlePut , this, std::placeholders::_1));
	m_pListener->support(methods::DEL,  std::bind(&CRestServer::handleDel , this, std::placeholders::_1));
	try
	{
		m_pListener
			->open()
			.then([]() {TRACE(L"\nstarting to listen\n"); })
			.wait();

		while (true);
	}
	catch (std::exception const & e)
	{
		strRes = e.what();
	}

	if (m_pListener) delete m_pListener;
	m_pListener = nullptr;
	return strRes;
}