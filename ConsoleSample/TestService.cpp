#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define NTDDI_VERSION NTDDI_WIN7

#include "TCPclient.h"

/* C++ & STD */
#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>
#include <codecvt>
#include <fstream>
#include <ctime>
#include <filesystem>

/* BOOST */
#include <boost/asio/connect.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/system/system_error.hpp>
#include <boost/asio/write.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

/* CPP_REST */
#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#include <cpprest/http_client.h>
#include <cpprest/containerstream.h>
#include <cpprest/filestream.h>
#include "multipart_parser.h"

/* C Header */
#include <stdlib.h>
#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>
#include <tlhelp32.h>
#include <shellapi.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace concurrency::streams;
using boost::asio::deadline_timer;
using boost::asio::ip::tcp;
using boost::lambda::bind;
using boost::lambda::var;
using boost::lambda::_1;


#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))


#define D_SEP L"\\"
#define D_UNDER L"_"
#define D_ARGS_INDEX_SEP L", "
#define D_ARGS_LINE_SEP L"\n"
#define D_VAR_NUM L"99"

//////////////////////////////////////////////////////////////////////////

class SocConfig {
public:
	SocConfig();
	~SocConfig(void);

	std::wstring Get(std::wstring strOptName);
	void SetDefault(std::wstring strModelFile);
	void SetWorkingDir(const int nIDX, const int nTgt, const int nNum);
	void ReadFromFile();
	std::wstring GetWorkingDir();
	std::wstring GetWorkingMecInputFile();
	std::wstring GetWorkingMecOutputFile();
	std::wstring GetWorkingCivilInputFile();
private:
	json::value m_jv;
	std::wstring strTime;
	std::wstring strWorkingDir;

	void GetTimeInfo(std::wstring& strTime);
protected:
	
};

SocConfig::SocConfig()
{
	GetTimeInfo(strTime);
	m_jv = json::value::object();
	//SetDefault(strModelFile);
}

void SocConfig::GetTimeInfo(std::wstring& strTime)
{
	struct tm curTM;
	time_t current_time = time(nullptr);

	time_t curTime = time(NULL);

	// Convert the current time 
	struct tm *pLocal = NULL;
#if defined(_WIN32) || defined(_WIN64) 
	localtime_s(&curTM, &curTime);
#else 
	localtime_r(&curTime, pLocal);
#endif 
	wchar_t buf[256];
	wcsftime(buf, sizeof(buf), L"%Y%m%d%H%M%S", &curTM);

	strTime = buf;
}

SocConfig::~SocConfig(void)
{

}

std::wstring SocConfig::Get(std::wstring strOptName) {
	if (m_jv.has_field(strOptName)) {
		return m_jv[strOptName].as_string();
	}
	else {
		// throw error
		return L"";
	}
}

void SocConfig::SetWorkingDir(const int nIDX, const int nTgt, const int nNum)
{
	strWorkingDir = Get(L"WorkingRoot") + D_SEP + std::to_wstring(nIDX) + D_UNDER + std::to_wstring(nTgt) + D_UNDER + std::to_wstring(nNum) + D_UNDER + strTime;
}

void SocConfig::ReadFromFile()
{
	// file exist check
	std::wfstream wof_conf;
	std::wstring strModelFile = Get(L"InitalModelFile");
	std::size_t nLastSep = strModelFile.find_last_of(L"\\");
	std::wstring strModelPath = strModelFile.substr(0, nLastSep);
	//std::wstring strConfig = strModelPath + D_SEP + L"config.json"; // optional
	WCHAR path[MAX_PATH];
	DWORD dLen = GetModuleFileNameW(NULL, path, MAX_PATH);
	std::wstring strExePath = path;
	std::wstring strDirectory;
	const size_t last_slash_idx = strExePath.rfind('\\');
	if (std::string::npos != last_slash_idx)
	{
		strDirectory = strExePath.substr(0, last_slash_idx);
	}
	
	std::wstring strConfig = strDirectory + L"\\config.json"; // optional
	std::wcout << L"config path: " << strConfig << std::endl;

	wof_conf.open(strConfig, std::ios_base::in | std::ios_base::app);
	json::value jv_load = json::value::parse(wof_conf);
	
	auto dataObj = jv_load.as_object();

	for (auto iterInner = dataObj.cbegin(); iterInner != dataObj.cend(); ++iterInner)
	{
		m_jv[iterInner->first] = jv_load.at(iterInner->first);
	}
}

void SocConfig::SetDefault(std::wstring strModelFile) {
	const std::wstring strNS_PostPix = L"eigen";  // Define
	const std::wstring strMec_PostPix = L"Eigen Value Case";  // From Civil Code

	std::size_t nLastSep = strModelFile.find_last_of(L"\\");
	std::wstring strModelPath = strModelFile.substr(0, nLastSep);
	std::wstring strModelName = strModelFile.substr(nLastSep + 1, strModelFile.length() - nLastSep - 4 - 1);

	m_jv[L"InitalModelFile"] = json::value::string(strModelFile);
	m_jv[L"InitalResultFile"] = json::value::string(strModelPath + D_SEP + L"InitalResult.json"); // optional
	m_jv[L"WorkingRoot"] = json::value::string(strModelPath); // optional
	m_jv[L"MecInputFile"] = json::value::string(strModelPath + D_SEP + strModelName + D_UNDER + strNS_PostPix + D_UNDER + strMec_PostPix + L".mec"); // optional
	m_jv[L"MecModelFile"] = json::value::string(strModelPath + D_SEP + strModelName + D_UNDER + strNS_PostPix + L".mcb"); // optional
	m_jv[L"MecRunOpt"] = json::value::string(L"-iMec2009 --runbyopt=on"); // optional
	//m_jv[L"MecSolver"] = json::value::string(L"D:\\MIDAS\\KT\\ConsoleSample\\mecsolver_x64\\MecSolver.exe");
	//m_jv[L"CivilNS"] = json::value::string(L"D:\\MIDAS\\KT\\genw_NS_KT\\bin\\CivilDebug\\x64\\CVLw.exe");
	m_jv[L"CivilNSHost"] = json::value::string(L"localhost"); // optional
	m_jv[L"CivilNSPort"] = json::value::string(L"27015"); // optional
	//m_jv[L"Civil900"] = json::value::string(L"D:\\MIDAS\\KT\\genw_900KT\\bin\\CivilDebug\\x64\\CVLw.exe");
	m_jv[L"Civil900Host"] = json::value::string(L"localhost"); // optional
	m_jv[L"Civil900Port"] = json::value::string(L"27014"); // optional
	m_jv[L"ResultUri"] = json::value::string(L"https://enipdkvyhyqbqby.m.pipedream.net"); // optional
}

std::wstring SocConfig::GetWorkingDir() {
	return strWorkingDir;
}

std::wstring SocConfig::GetWorkingMecInputFile() {
	std::wstring strOrigin = Get(L"MecInputFile");
	std::size_t nLastSep = strOrigin.find_last_of(L"\\") + 1;
	std::wstring strOriginFile = strOrigin.substr(nLastSep, strOrigin.length() - nLastSep);
	return GetWorkingDir() + D_SEP + strOriginFile;
}


std::wstring SocConfig::GetWorkingMecOutputFile()
{
	std::wstring strOrigin = Get(L"MecInputFile");
	std::size_t nLastSep = strOrigin.find_last_of(L"\\") + 1;
	std::wstring strOriginFile = strOrigin.substr(nLastSep, strOrigin.length() - nLastSep - 4);
	return GetWorkingDir() + D_SEP + strOriginFile + L".out";
}

std::wstring SocConfig::GetWorkingCivilInputFile() {
	std::wstring strOrigin = Get(L"InitalModelFile");
	std::size_t nLastSep = strOrigin.find_last_of(L"\\") + 1;
	std::wstring strOriginFile = strOrigin.substr(nLastSep, strOrigin.length() - nLastSep);
	return GetWorkingDir() + D_SEP + strOriginFile;
}

//////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------
struct T_EIGEN_D
{
	int nNum;
	std::vector<double> vdFreq;
	std::wstring stResult;
};

struct T_ANAL_ARGS_D
{
	std::wstring command;
	std::wstring data[8];

	std::wstring ToText()
	{
		std::wstring output;
		if (command.empty()) output.append(L"       ,");
		else output.append(command + L", ");
		for (int i = 0; i < 8; i++)
		{
			output.append(data[i]);
			if (i != 7) output.append(D_ARGS_INDEX_SEP);
			else output.append(D_ARGS_LINE_SEP);
		}

		return output;
	}
};

std::wstring MakeEigenAnalData(T_EIGEN_D& data)
{
	std::wstring strRes;
	strRes.append(L"PARAM, MODEPFACTOR, -1\n");

	std::vector<T_ANAL_ARGS_D> vargs;
	std::size_t nModeCount = data.vdFreq.size();
	vargs.resize(nModeCount + 5);

	// COMMAND : ELMPARM
	{
		vargs[0].command = L"ELMPARM";
		vargs[0].data[0] = L"1";

		vargs[1].data[0] = L"EIGEN";

		for (std::size_t i = 0; i < nModeCount; i++)
		{
			vargs[i + 2].data[0] = std::to_wstring(i + 1);
			vargs[i + 2].data[3] = std::to_wstring(data.vdFreq[i]);
		}

		vargs[nModeCount + 2].data[0] = L"DVAR";

		vargs[nModeCount + 3].data[0] = D_VAR_NUM;
		vargs[nModeCount + 3].data[1] = L"1.e+05";
		vargs[nModeCount + 3].data[2] = L"1.e+15";
	}

	// COMMAND : DVAR
	vargs[nModeCount + 4].command = L"DVAR";
	vargs[nModeCount + 4].data[0] = D_VAR_NUM;
	vargs[nModeCount + 4].data[1] = L"MAT1";
	vargs[nModeCount + 4].data[2] = L"1";
	vargs[nModeCount + 4].data[3] = L"E";

	for (auto args : vargs)
	{
		strRes.append(args.ToText());
	}

	return strRes;
}

void MoveFileLastLine(std::wfstream& wfs, int& nLastPos)
{
	nLastPos = -1;
	if (wfs.is_open())
	{
		wfs.seekg(-1, std::ios_base::end);

		bool bFirstData = false;
		while (true) {
			wchar_t ch;
			wfs.get(ch);

			if ((int)wfs.tellg() <= 1) {
				wfs.seekg(0);
				break;
			}
			else if (ch == L'\n') {
				if (bFirstData) break;
				wfs.seekg(-3, std::ios_base::cur);
			}
			else {
				wfs.seekg(-2, std::ios_base::cur);
				bFirstData = true;
			}
		}

		nLastPos = static_cast<int>(wfs.tellg());
	}
}

bool CopyDirectory(boost::filesystem::path const & source, boost::filesystem::path const & destination)
{
	namespace fs = boost::filesystem;
	try
	{
		if (!fs::exists(source) || !fs::is_directory(source))
		{
			std::cout << "Source directory " << source.string() << " does not exist or is not a directory." << std::endl;
			return false;
		}
		if (fs::exists(destination))
		{
			std::cout << "Destination directory " << destination.string() << " already exists." << std::endl;
			return false;
		}
		if (!fs::create_directory(destination))
		{
			std::cout << "Unable to create destination directory" << destination.string() << std::endl;
			return false;
		}
	}
	catch (fs::filesystem_error const & e)
	{
		std::cerr << e.what() << std::endl;
		return false;
	}

	for (fs::directory_iterator file(source); file != fs::directory_iterator(); ++file)
	{
		try
		{
			fs::path current(file->path());
			if (fs::is_directory(current))
			{
				if (!CopyDirectory(current, destination / current.filename()))
				{
					return false;
				}
			}
			else
			{
				fs::copy_file(current, destination / current.filename());
			}
		}
		catch (fs::filesystem_error const & e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	return true;
}

//
// https://pipedream.com/@aplgreenh/p_BjCkaan/edit?h=eyJ2IjoxLCJjIjpbInNfWTUzQ3pyIixbIlwvKiBUbyBnZXQgc3RhcnRlZDpcclxuXHJcbjEuIFNlbmQgSFRUUCBvciB3ZWJob29rIHJlcXVlc3RzIHRvIHRoZSBlbmRwb2ludCBVUkwgYWJvdmUgKGUuZy4sIGh0dHBzOlwvXC9lblhYWFhYWFhYLm0ucGlwZWRyZWFtLm5ldClcclxuMi4gU2VsZWN0IGV2ZW50cyB0byBpbnNwZWN0IHRoZW0uXHJcbjMuIE9wdGlvbmFsbHkgZWRpdCB0aGUgY29kZSBiZWxvdyB0byBjdXN0b21pemUgdGhlIHJlc3BvbnNlLlxyXG5cclxuVGhlbiBleHBsb3JlIGhvdyBQaXBlZHJlYW0gY2FuIGhlbHAgeW91IGNvbm5lY3QgQVBJcywgcmVtYXJrYWJseSBmYXN0OlxyXG5cclxuLSBDb25uZWN0IE9BdXRoIGFuZCBrZXktYmFzZWQgQVBJIGFjY291bnRzIGluIHNlY29uZHMuXHJcbi0gVXNlIGNvbm5lY3RlZCBhY2NvdW50cyBpbiBOb2RlLmpzIGNvZGUgc3RlcHMgKHdpdGggbnBtKSBvciBuby1jb2RlIGJ1aWxkaW5nIGJsb2Nrcy5cclxuLSBCdWlsZCBhbmQgcnVuIHdvcmtmbG93cyB0cmlnZ2VyZWQgb24gSFRUUCByZXF1ZXN0cywgc2NoZWR1bGVzLCBhcHAgZXZlbnRzIGFuZCBtb3JlLlxyXG5cclxuTGVhcm4gbW9yZSBhdCBodHRwczpcL1wvcGlwZWRyZWFtLmNvbVwvcXVpY2tzdGFydCAqXC9cclxuXHJcbmNvbnN0IGJvZHkgPSB7XHJcbiAgYWJvdXQ6IGBQaXBlZHJlYW0gaXMgYSBwcm9kdWN0aW9uLXNjYWxlIHNlcnZlcmxlc3MgcGxhdGZvcm0gdG8gY29ubmVjdCBBUElzIHJlbWFya2FibHkgZmFzdC5gLFxyXG4gIGV2ZW50X2lkOiBzdGVwcy50cmlnZ2VyLmNvbnRleHQuaWQsXHJcbiAgd29ya2Zsb3dfaWQ6IHN0ZXBzLnRyaWdnZXIuY29udGV4dC53b3JrZmxvd19pZCxcclxuICBvd25lcl9pZDogc3RlcHMudHJpZ2dlci5jb250ZXh0Lm93bmVyX2lkLFxyXG4gIGRlcGxveW1lbnRfaWQ6IHN0ZXBzLnRyaWdnZXIuY29udGV4dC5kZXBsb3ltZW50X2lkLFxyXG4gIHRpbWVzdGFtcDogc3RlcHMudHJpZ2dlci5jb250ZXh0LnRzLFxyXG4gIGluc3BlY3Q6IGBodHRwczpcL1wvcGlwZWRyZWFtLmNvbVwvQFwvJHtzdGVwcy50cmlnZ2VyLmNvbnRleHQud29ya2Zsb3dfaWR9YCwgXHJcbiAgcXVpY2tzdGFydDogYGh0dHBzOlwvXC9waXBlZHJlYW0uY29tXC9xdWlja3N0YXJ0XC9gLFxyXG59XHJcblxyXG4kcmVzcG9uZCh7XHJcbiAgc3RhdHVzOiAyMDAsXHJcbiAgYm9keSwgXHJcbn0pXHJcblxyXG5yZXR1cm4gYm9keSJdXX0%3D&utm_source=requestbin.com&utm_medium=referral&utm_campaign=home&autodeploy=1&_ga=2.143047925.1752399860.1624246993-164896925.1624246993&e=1uF2ZJZtelIaBNYrEfHr1X7DYl1
// https://stackoverflow.com/questions/42370221/http-client-post-request-using-c-rest-sdk-casablanca
std::wstring GetResultStr(const bool bRes){
	if (bRes) { return L"OK"; }
	else { return L"NOK"; }
}

void PostResult(SocConfig& rSocConfig, int nIdx) {
	bool bRes = true;

	json::value jv = json::value::object();
	//jv[L"TWIN_IDX"] = web::json::value::number(nidx);
	jv[L"TWIN_IDX"] = web::json::value::string(std::to_wstring(nIdx));
	jv[L"result"] = web::json::value::string(GetResultStr(bRes));

	http_client client(rSocConfig.Get(L"ResultUri"));
	//http_request req();

	client.request(methods::POST, L"/", jv).then([=](http_response r) {
		std::wcout << U("PostResult - STATUS : ") << r.status_code() << std::endl;

		r.extract_json(true).then([](json::value v) {
			//std::wcout << v.at(U("date")).as_string() << std::endl;
			//std::wcout << v.at(U("time")).as_string() << std::endl;
		}).wait();
	}).wait();
}

void ExecuteProcessSync(std::wstring process, std::wstring param)
{
	SHELLEXECUTEINFO ShExecInfo = { 0 };
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
	ShExecInfo.lpFile = process.c_str();
	ShExecInfo.lpParameters = param.c_str();
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_SHOW;
	ShExecInfo.hInstApp = NULL;
	ShellExecuteEx(&ShExecInfo);
	WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
	CloseHandle(ShExecInfo.hProcess);
}

bool GenerateOriginMecFile(SocConfig& rSocConfig) {
	TCPclient cNS;
	if (!boost::filesystem::exists(rSocConfig.Get(L"MecInputFile")))
	{
		// Run Civil NS
		::ShellExecute(NULL, L"open", rSocConfig.Get(L"CivilNS").c_str(), rSocConfig.Get(L"MecModelFile").c_str(), NULL, SW_SHOWNORMAL);
		cNS.Init("CVLw.exe", WStringToString(rSocConfig.Get(L"CivilNSHost")), WStringToString(rSocConfig.Get(L"CivilNSPort")));
		cNS.PingPong("EXPORT");
		cNS.PingPong("CLOSE");
		cNS.Close();
		return true;
	}
	return false;
}

bool GenerateWorkingMecFile(SocConfig& rSocConfig) {
	boost::filesystem::create_directories(rSocConfig.GetWorkingDir());
	//rSocConfig.GetWorkingDir(nIDX, nTgt, rEigenD.nNum) + 
	//boost::filesystem::copy(rSocConfig.Get(L"MecInputFile"), rSocConfig.GetWorkingMecInputFile());
	return true;
}

bool RemoveWorkingDirectory(SocConfig& rSocConfig) {
	boost::filesystem::remove_all(rSocConfig.GetWorkingDir());
	return true;
}

bool UpdateMecFile(SocConfig& rSocConfig, T_EIGEN_D& rEigenD) {
	std::wfstream wif_mec;
	wif_mec.open(rSocConfig.Get(L"MecInputFile"), std::ios_base::in | std::ios_base::out);

	std::vector<std::wstring> vtBuf;
	if (wif_mec.is_open())
	{
		while (!wif_mec.eof())
		{
			std::wstring wstrLine;
			std::getline(wif_mec, wstrLine);
			vtBuf.push_back(wstrLine);
		}
		wif_mec.close();
	}
	else {
		return false;
	}

	std::wstring strMecEigenData = MakeEigenAnalData(rEigenD);
	
	for (int i = 0; i < vtBuf.size(); ++i ) {
		std::size_t nFindPos = vtBuf[i].find(L"USE(EIGSOL)");
		if (nFindPos != std::wstring::npos) {
			vtBuf.erase(vtBuf.begin() + i);
		}
	}

	for (int i = 0; i < vtBuf.size(); ++i) {
		std::size_t nFindPos = vtBuf[i].find(L"SOL 103");
		if (nFindPos != std::wstring::npos) {
			vtBuf[i] = L"SOL 1103";
		}
	}

	for (int i = 0; i < vtBuf.size(); ++i) {
		std::size_t nFindPos = vtBuf[i].find(L"ENDDATA");
		if (nFindPos != std::wstring::npos) {
			vtBuf[i] = strMecEigenData;
			vtBuf.push_back(L"ENDDATA");
			break;
		}
	}

	std::ofstream wif_mec_wr;
	wif_mec_wr.open(rSocConfig.GetWorkingMecInputFile(), std::ios_base::out | std::ios_base::trunc);

	if (wif_mec_wr.is_open()) {
		for (auto sLine : vtBuf) {
			std::string sLineBuf;
			sLineBuf.assign(sLine.begin(), sLine.end());
			wif_mec_wr << sLineBuf << std::endl;
		}

		wif_mec_wr.flush();
		wif_mec_wr.close();
	}
	else {
		std::wcout << L"file write fail";
		return false;
	}
	
	return true;
}

bool ExtractElast(SocConfig& rSocConfig, double& rdElast) {
	std::wfstream wif_mec_res;
	wif_mec_res.open(rSocConfig.GetWorkingMecOutputFile(), std::ios_base::in | std::ios_base::out);
	if (wif_mec_res.is_open())
	{
		while (!wif_mec_res.eof())
		{
			std::wstring wstrLine;
			std::getline(wif_mec_res, wstrLine);

			std::size_t nResPos = wstrLine.find(D_VAR_NUM);
			if (nResPos != std::wstring::npos)
			{
				std::wstring strRes = wstrLine.substr(nResPos + 2, wstrLine.length() - nResPos - 2);
				boost::trim(strRes);
				rdElast = boost::lexical_cast<double>(strRes) * 0.000001; // N/M^2 -> N/MM^z
				return true;
			}
		}
		wif_mec_res.close();
	}
	return false;
}

bool ExcuteCivilAnalysis(SocConfig& rSocConfig, T_EIGEN_D& curData, double dElast, int nTargetSpan, double dInitalFactor) {
	boost::filesystem::copy(rSocConfig.Get(L"InitalModelFile"), rSocConfig.GetWorkingCivilInputFile());
	::ShellExecute(NULL, L"open", rSocConfig.Get(L"Civil900").c_str(), rSocConfig.GetWorkingCivilInputFile().c_str(), NULL, SW_SHOWNORMAL);

	TCPclient c900;

	{
		c900.Init("CVLw.exe", WStringToString(rSocConfig.Get(L"Civil900Host")), WStringToString(rSocConfig.Get(L"Civil900Port")));
		json::value jv = json::value::object();
		jv[L"command"] = json::value::string(L"ANAL");
		jv[L"elast"] = dElast;


		std::string strSend = WStringToString(jv.serialize());
		std::string strRecv = c900.PingPong(strSend);
	}

	std::wstring strResPath = curData.stResult;
	json::value jv_res = json::value::object();

	{
		boost::filesystem::create_directories(strResPath);

		json::value jv = json::value::object();
		jv[L"command"] = json::value::string(L"RESULT");
		jv[L"target"] = nTargetSpan;
		jv[L"path"] = json::value::string(strResPath);

		std::string strSend = WStringToString(jv.serialize());
		std::string strRecv = c900.PingPong(strSend);
		
		jv_res[std::to_wstring(curData.nNum)] = json::value::parse(StringToWString(strRecv));
		jv_res[std::to_wstring(curData.nNum)][L"earlyLoadFactor"] = dInitalFactor;
	}

	std::ofstream wof_res;
	std::wstring strResName = strResPath + D_SEP + L"result.json";
	wof_res.open(strResName, std::ios_base::out | std::ios_base::trunc);

	if (wof_res.is_open())
	{
		std::wstring resContent = jv_res[std::to_wstring(curData.nNum)].serialize();
		std::string resBuf;
		resBuf.assign(resContent.begin(), resContent.end());
		wof_res.write(resBuf.c_str(), resBuf.length());
		wof_res.close();
	}

	{
		json::value jv = json::value::object();
		jv[L"command"] = json::value::string(L"CLOSE");

		std::string strSend = WStringToString(jv.serialize());
		std::string strRecv = c900.PingPong(strSend);
		c900.Close();
	}

	return true;
}

bool ParseInitalResult(SocConfig& rConfig, const int nTargetSpan, double& rValue) {
	std::wfstream wof_res;
	wof_res.open(rConfig.Get(L"InitalResultFile"), std::ios_base::in | std::ios_base::app);
	json::value jv_inital = json::value::parse(wof_res);
	std::wstring wstrSpan = L"Span" + std::to_wstring(nTargetSpan);
	rValue = jv_inital[wstrSpan].as_double();
	return true;
}

bool DownloadFile(std::wstring sFilePath, std::wstring sProject, std::wstring sBranch, std::wstring sSha) {
	auto fileStream = std::make_shared<ostream>();

	pplx::task<void> requestTask = fstream::open_ostream(sFilePath).then([=](ostream outFile)
	{
		*fileStream = outFile;

		// Create http_client to send the request.
		http_client client(U("http://localhost:7700/api/file/get/"));

		// Build request URI and start the request.
		uri_builder builder(sProject + U("/"));
		builder.append_query(U("branch"), sBranch);
		builder.append_query(U("commit"), sSha);
		return client.request(methods::GET, builder.to_string());
	})

		// Handle response headers arriving.
		.then([=](http_response response)
	{
		printf("Received response status code:%u\n", response.status_code());

		// Write response body into the file.
		return response.body().read_to_end(fileStream->streambuf());
	})

		// Close the file stream.
		.then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
		return false;
	}
	return true;
}

bool UploadFile(std::wstring sFilePath, std::wstring sName, std::wstring sProject, std::wstring sBranch, std::wstring sSha) {
	auto fileStream = std::make_shared<ostream>();

	pplx::task<void> requestTask = fstream::open_ostream(U("ResultUU")).then([=](ostream outFile)
	{
		*fileStream = outFile;

		std::string strFilePath;
		strFilePath.assign(sFilePath.begin(), sFilePath.end());
		std::string strName;
		strName.assign(sName.begin(), sName.end());
		//Use MultipartParser to get the encoded body content and boundary
		MultipartParser parser;
		parser.AddParameter("workMessage", strName);  // 파라미터
		parser.AddFile("cimProject", strFilePath); // 실제 파일이 담기는 이름
		std::string boundary = parser.boundary();
		std::string body = parser.GenBodyContent();
		std::cout << body << std::endl;

		std::wstring sUri = U("http://localhost:7700/api/file/uploadq/") + sProject + U("/") + 
			U("?branch=") + sBranch +
			U("&commit=") + sSha;
		http_client client(sUri);

		http_request req;
		req.set_method(web::http::methods::POST);
		req.set_body(body, "multipart/form-data; boundary=" + boundary);
		
		return client.request(req);
	})
		.then([=](pplx::task<http_response> response_task)
	{
		http_response response = response_task.get();
		return response.body().read_to_end(fileStream->streambuf());
	})
		.then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
		return false;
	}
	return true;
}

int main(int argc, char* argv[])
{
	/*
	std::wstring sFilePath = U("D:\\MIDAS\\KT\\ktsoc\\ConsoleSample\\results.cimz");
	std::wstring sFilePathU = U("D:\\MIDAS\\KT\\ktsoc\\ConsoleSample\\resultsu.cimz");
	std::wstring sPrject = U("project1");
	std::wstring sBranch = U("2nd");
	std::wstring sSha = U("39a47be");
	std::wstring sLog = U("worklog!!");
	DownloadFile(sFilePath, sPrject, sBranch, sSha);
	UploadFile(sFilePathU, sLog, sPrject, sBranch, sSha);

	return 0;
	*/
	/*
	auto fileStream = std::make_shared<ostream>();

	// Open stream to output file.
	pplx::task<void> requestTask = fstream::open_ostream(U("results")).then([=](ostream outFile)
	{
		*fileStream = outFile;

		//Use MultipartParser to get the encoded body content and boundary
		MultipartParser parser;
		parser.AddParameter("Filename", "project1.cimz");
		parser.AddFile("file", "project1.cimz");
		std::string boundary = parser.boundary();
		std::string body = parser.GenBodyContent();
		std::cout << body << std::endl;

		//Set up http client and request
		http_request req;
		http_client client(U("http://localhost:7700/api/file/get/?branch=2nd&commit=39a47be"));
		req.set_method(web::http::methods::GET);
		req.set_body(body, "multipart/form-data; boundary=" + boundary);
		return client.request(req);
	})
		.then([=](pplx::task<http_response> response_task)
	{
		http_response response = response_task.get();
		return response.body().read_to_end(fileStream->streambuf());
	})
		.then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}

	return 0;
	*/

	std::locale::global(std::locale(".UTF-8"));

	std::wstring wstrCommandPath = StringToWString(argv[1]);
	std::wifstream wif;
	wif.open(wstrCommandPath, std::ios_base::in);

	try
	{
		std::vector<T_EIGEN_D> vData;
		int nTargetSpan;
		double dNewElast = 10;
		double dEarlyFactor = 0;
		int nTwinIdx = 0;

		std::wstring strModelFile;

		SocConfig sConfig;
		

		if (wif.is_open())
		{
			while (!wif.eof())
			{
				std::wstring wstrLine;
				std::getline(wif, wstrLine);

				std::size_t nDot = wstrLine.find('.');
				std::size_t nEq = wstrLine.find('=');
				std::size_t nOp = wstrLine.find('(');
				std::size_t nCp = wstrLine.find(')');

				std::wstring strCtg = wstrLine.substr(0, nDot);
				std::wstring strCmd;
				std::wstring strArg;

				if (nEq != std::string::npos)
				{
					strCmd = wstrLine.substr(nDot + 1, nEq - nDot - 1);
					strArg = wstrLine.substr(nEq + 1, wstrLine.length() - nEq - 1);
				}
				else if (nOp != std::string::npos && nCp != std::string::npos)
				{
					strCmd = wstrLine.substr(nDot + 1, nOp - nDot - 1);
					strArg = wstrLine.substr(nOp + 1, nCp - nOp - 1);
				}
				else
					break;

				if (strCtg == L"DOC")
				{
					if (strCmd == L"OPEN")
					{
						strModelFile = strArg;
					}
					else if (strCmd == L"ANAL")
					{
						//
					}
					else if (strCmd == L"RESULT")
					{
						
						std::wcout << L"SetConfig" << std::endl;
						sConfig.SetDefault(strModelFile);
						sConfig.ReadFromFile();
						vData.back().stResult = strArg;
						T_EIGEN_D& curData = vData.back();
						std::wcout << L"SetWorkingDir" << std::endl;
						sConfig.SetWorkingDir(nTwinIdx, nTargetSpan, curData.nNum);
						std::wcout << L"ParseInitalResult" << std::endl;
						ParseInitalResult(sConfig, nTargetSpan, dEarlyFactor);
						std::wcout << L"GenerateOriginMecFile" << std::endl;
						GenerateOriginMecFile(sConfig);
						std::wcout << L"GenerateWorkingMecFile" << std::endl;
						GenerateWorkingMecFile(sConfig);
						std::wcout << L"UpdateMecFile" << std::endl;
						UpdateMecFile(sConfig, curData);

						std::wcout << L"ExecuteProcessSync(MecSolver)" << std::endl;
						std::wstring strSolverArg;
						strSolverArg.append(L"\"" + sConfig.GetWorkingMecInputFile() + L"\" " + sConfig.Get(L"MecRunOpt"));
						ExecuteProcessSync(sConfig.Get(L"MecSolver"), strSolverArg);

						std::wcout << L"ExtractElast" << std::endl;
						if (!ExtractElast(sConfig, dNewElast)) {
							assert(false);
						}

						std::wcout << L"ExcuteCivilAnalysis" << std::endl;
						ExcuteCivilAnalysis(sConfig, curData, dNewElast, nTargetSpan, dEarlyFactor);
						RemoveWorkingDirectory(sConfig);
					}
					else if (strCmd == L"CLOSE")
					{
						std::wcout << L"PostResult" << std::endl;
						PostResult(sConfig, nTwinIdx);
					}
				}
				else if (strCtg == L"VAR")
				{
					if (strCmd == L"NUM")
					{
						T_EIGEN_D data;
						data.nNum = std::stoi(strArg);
						if (data.nNum <= 0) throw std::runtime_error(std::string("NUM must be greater than 0"));
						vData.push_back(data);
					}
					else if (strCmd.find(L"FREQ") != std::wstring::npos)
					{
						double dFreq = std::stod(strArg);
						vData.back().vdFreq.push_back(dFreq);
					}
					else if (strCmd == L"TARGET")
					{
						nTargetSpan = std::stoi(strArg);
					}
					else if (strCmd == L"TWIN_IDX")
					{
						nTwinIdx = std::stoi(strArg);
					}
				}
			}
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	wif.close();
	return 0;
}